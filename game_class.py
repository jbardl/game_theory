import numpy as np
import pandas as pd


class Player:
    def __init__(self, name, strategies, utility_function=None):
        self.name = name
        self.strategies = strategies
        self.utility_function = utility_function

    def get_outcomes(self, game):
        my_index = game.players_names.index(self.name)
        outcomes_set = set()
        output = list()
        for strategy in game.outcomes:
            for element in strategy:
                if type(element) == tuple:
                    outcomes_set.add(element[my_index])
                else:
                    outcomes_set.add(element)
        outcomes = list(outcomes_set)
        for n in range(len(outcomes)):
            max_el = max(outcomes) # voy a querer que sea max en el caso de utility_func greedy
            index = outcomes.index(max_el)
            output.append(outcomes.pop(index))
        return output

    def greedy_utility_function(self, outcome, game):
        my_index = game.players_names.index(self.name)
        outcomes = self.get_outcomes(game)
        ranking = np.arange(len(outcomes))
        utility = dict()
        for i, game_outcome in enumerate(outcomes):
            utility.update({game_outcome: ranking[i]})
        for n in utility:
            if n == outcome[my_index]:
                return utility[n]

    def get_weakly_dominant_strategies(self, game):
        strategies = [strategy for strategy in self.strategies
                      if game.weakly_dominant(self.name, strategy)]
        return strategies

    def get_strictly_dominant_strategies(self, game):
        strategies = [strategy for strategy in self.strategies
                      if game.strictly_dominant(self.name, strategy)]
        return strategies


class Game:
    def __init__(self, players, outcomes):
        self.players = players
        self.players_names = [player.name for player in self.players]
        self.outcomes = outcomes
        self.index = pd.MultiIndex.from_product([[self.players[0].name], self.players[0].strategies])
        self.columns = pd.MultiIndex.from_product([[self.players[1].name], self.players[1].strategies])
        self.df = pd.DataFrame(data=self.outcomes, index=self.index, columns=self.columns)

    def utility_function(self, x):
        return (self.players[0].utility_function(x), self.players[1].utility_function(x))

    def apply_utility_function(self):
        reduced_form = self.df.applymap(self.utility_function)
        return Game(self.players, reduced_form.values)

    def greedy_utility_function(self, x):
        return (self.players[0].greedy_utility_function(x, self), self.players[1].greedy_utility_function(x, self))

    def apply_greedy_utility(self):
        reduced_form = self.df.applymap(self.greedy_utility_function)
        output = Game(self.players, reduced_form.values)
        for player in output.players:
            player.utility_function = player.greedy_utility_function
        return output

    def apply_conversion(self, function):
        reduced_form = self.df.applymap(function)
        return Game(self.players, reduced_form.values)

    def get_player_outcomes(self, player, strategy_a, strategy_b):
        player_index = self.players_names.index(player)
        profiles_x_strategy = list()
        player_outcomes = list()
        # profiles_x_strategy = [ [ [out1, out2],[out1, out2] ] , [ [out1, out2],[out1, out2] ] ]
        if player_index == 0:
            profiles_x_strategy.append(self.df.loc[player].loc[strategy_a].values)
            profiles_x_strategy.append(self.df.loc[player].loc[strategy_b].values)
        elif player_index == 1:
            profiles_x_strategy.append(self.df[player][strategy_a].values)
            profiles_x_strategy.append(self.df[player][strategy_b].values)
        # strategy_profiles = [ [out1, out2] , [out1, out2] ]
        for strategy_profiles in profiles_x_strategy:
            strategy_outcomes = list()
            # profile = [out1, out2]
            for profile in strategy_profiles:
                strategy_outcomes.append(profile[player_index])
            player_outcomes.append(strategy_outcomes)
        return player_outcomes[0], player_outcomes[1]

    def strict_dominance(self, player, strategy_a, strategy_b, verb=False):
        outcomes_a, outcomes_b = self.get_player_outcomes(player, strategy_a, strategy_b)
        is_dominant = True
        if len(outcomes_a) == len(outcomes_b):
            length_outcomes = len(outcomes_a)
        else:
            print('Strategies length dont match')
            return None
        for n in range(length_outcomes):
            if outcomes_a[n] <= outcomes_b[n]:
                is_dominant = False
        if verb:
            if is_dominant:
                print('domina')
            else:
                print('no domina')
        return is_dominant

    def weak_dominance(self, player, strategy_a, strategy_b, verb=False):
        outcomes_a, outcomes_b = self.get_player_outcomes(player, strategy_a, strategy_b)
        is_dominant = True
        if len(outcomes_a) == len(outcomes_b):
            length_outcomes = len(outcomes_a)
        else:
            print('Strategies length dont match')
            return None
        for n in range(length_outcomes):
            if outcomes_a[n] < outcomes_b[n]:
                is_dominant = False
        if verb:
            if is_dominant == True:
                print('domina')
            else:
                print('no domina')
        return is_dominant

    def weakly_dominant(self, player, strategy, verb=False):
        is_weakly_dominant = True
        player_index = self.players_names.index(player)
        for other_strategy in self.players[player_index].strategies:
            if strategy != other_strategy:
                if not self.weak_dominance(player, strategy, other_strategy):
                    is_weakly_dominant = False
        if verb:
            if is_weakly_dominant:
                print('débilmente dominante')
            else:
                print('no es dominante')
        return is_weakly_dominant

    def strictly_dominant(self, player, strategy, verb=False):
        is_strictly_dominant = True
        player_index = self.players_names.index(player)
        for other_strategy in self.players[player_index].strategies:
            if strategy != other_strategy:
                if not self.strict_dominance(player, strategy, other_strategy):
                    is_strictly_dominant = False
        if verb:
            if is_strictly_dominant:
                print('estrictamente dominante')
            else:
                print('no es dominante')
        return is_strictly_dominant