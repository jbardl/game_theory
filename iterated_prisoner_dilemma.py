from game_class import Game, Player
import numpy as np


class PlayerPD(Player):
    def __init__(self, name, strategies, utility_function=None):
        super().__init__(name, strategies, utility_function)
        self.name = name
        self.strategies = strategies
        self.utility_function = utility_function

    def tit_for_tat(self, choices, game_round, player_index):
        # en el primer round, divide
        if game_round == 1:
            return 'colaborar'
        # en todos los rounds posteriores, se fija en las decisiones ya tomadas y elije la misma que el otro
        else:
            return choices[game_round - 2][player_index]


class IterPD(Game):
    def __init__(self, players, outcomes):
        super().__init__(players, outcomes)

    def apply_utility_function(self):
        reduced_form = self.df.applymap(self.utility_function)
        return IterPD(self.players, reduced_form.values)

    def call_player_function(self):
        return self.players[0].getWeaklyDominantStrategies(self)

    def print_results(self, choices):
        results_x_round = list()
        totals_player_1 = 0
        totals_player_2 = 0
        for round_choices in choices:
            results_x_round.append(
                self.df.loc[self.players[0].name].loc[round_choices[0]][self.players[1].name][round_choices[1]])
        for result in results_x_round:
            totals_player_1 += result[0]
            totals_player_2 += result[1]
        print('Total puntos para {}: {}'.format(self.players[0].name, totals_player_1))
        print('Total puntos para {}: {}'.format(self.players[1].name, totals_player_2))

    def start(self, funcion_1, funcion_2, rounds):
        choices = list()
        for n in np.arange(1, rounds):
            if funcion_1 == 'choose_dominant':
                player_1_choice = self.players[0].get_weakly_dominant_strategies(self)[0]
            elif funcion_1 == 'tit_for_tat':
                player_1_choice = self.players[0].tit_for_tat(choices, n, 1)
            if funcion_2 == 'choose_dominant':
                player_2_choice = self.players[1].get_weakly_dominant_strategies(self)[0]
            elif funcion_2 == 'tit_for_tat':
                player_2_choice = self.players[1].tit_for_tat(choices, n, 0)
            choices.append([player_1_choice, player_2_choice])
        self.print_results(choices)
        return choices